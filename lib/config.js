module.exports = {
	//Add Address for your running meshblu instance
	skynet_server: "193.174.152.236",
	//Add port for your meshblu instance
	// where the gateway will try to connect
	skynet_port: 3005,
	//Just state a name for this gateway to differentiate it against other gateways
	//Further this config variable should be set when an owner exists
	gatewayName: "My Home Gateway",
	//This config variable is used to indicate an owner for this gateway
	//Standard gateblu gateways don't have any owner but if you want to connect a gateway for one specific user
	//the owner property is necessary
	//Default: false
	hasOwner: true,
	ownerUUID: "ea845bb0-3758-11e4-affe-9b0bd60043d0"
}