var when = require('when');

var skynet = require('skynet');

var subdevices = require('./subdevices');
var remoteConfig = require('./remoteConfig');
var config = require('./config');

var registerRetries = 0;
var retriesMax = 10;

function connect(gatewayId, token){
  var defer = when.defer();

  var skynetConfig = {
    uuid: gatewayId,
    token: token,
    server: config.skynet_server,
    port: config.skynet_port
  };

  console.log('connecting to skynet with', skynetConfig);

  var conn = skynet.createConnection(skynetConfig);
  conn.uuid = gatewayId;
  conn.token = token;

  var handlersRegistered = false;



  conn.on('notReady', function(data){
    console.log('UUID FAILED AUTHENTICATION!', data);
    if(!config.hasOwner){
      // Register device without any owner
      conn.register({
        uuid: gatewayId,
        token: token,
        type: 'gateway',
        name: gatewayName
      }, function (data) {
        console.log('registered', data);
        conn.emit('ready', data);
        defer.resolve(conn);
      });
    }else{
      // Register device with a specific owner
      conn.register({
        uuid: gatewayId,
        token: token,
        owner: config.ownerUUID,
        name: config.gatewayName,
        type: 'gateway'
      }, function (data) {
        console.log('registered', data);
        conn.emit('ready', data);
        defer.resolve(conn);
      });    
    }
  });

  conn.on('ready', function(data){
    defer.resolve(conn);
    console.log('UUID AUTHENTICATED!', data);
    // Subscribe to device/node i/o
      conn.subscribe({
        uuid: data.uuid,
        token: data.token
      },
        function(data){
          console.log("Susbcription success:" + data);
      });
    if(!handlersRegistered){
      conn.on('message', function(data, fn){
        console.log('\nmessage received from:', data.fromUuid, data);
        if(data.devices){

          var subdevice;
          if(data.devices === gatewayId){
            subdevice = data.subdevice;
          }else if(data.devices !== '*' && data.devices !== 'all'){
            //was messaged as hub_uuid/subdevice_id
            subdevice = data.devices;
          }


          try{
            //console.log(data);
            if(typeof data == "string"){
              data = JSON.parse(data);
            }

            if(subdevice){
              var instance = subdevices.instances[subdevice];

              if(instance && instance.onMessage){
                console.log('matching subdevice found:', subdevice);
                instance.onMessage(data, fn);
              }else{
                console.log('no matching subdevice:', subdevice);
              }
            }else{
              if(fn){
                console.log('responding');
                fn('hello back at you');
              }
            }

          }catch(exp){
            console.log('err dispatching message', exp);
          }

        }

      });

      // handle gateway configuration requests
      conn.on('config', function(data, cb){
        console.log('config api call received:', data, cb);
        remoteConfig(token, data, cb);
      });

      handlersRegistered = true;

    }else{
      console.log('hanlders already registered, just reconnected to skynet');
    }



    // Event triggered when device loses connection to skynet
    conn.on('disconnect', function(data){
      console.log('disconnected from skynet');
    });



    //WhoAmI?
    conn.whoami({uuid:gatewayId}, function (data) {
      console.log('whoami', data);
    });



  });

  return defer.promise;


}

module.exports = connect;
